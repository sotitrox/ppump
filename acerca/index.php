<?php
/************************************************************************\
*
*    PPump 0.3.1 Copyright 2014 Sotitrox
*    sotitrox@autistici.org
*
*    This file is part of PPump.
*
*    PPump is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    PPump is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
*
*    Public Pump o Pump Publico es programa escrito en PHP que permite
*    interpretar el canal web publico de la red Pump.io desde el sitio
*    ofirehose.com (https://ofirehose.com/feed.json) en formato Json 
*    para su fácil lectura.
*    Para información de su uso visite:
*    http://wiki.redaustral.tk/wikka.php?wakka=PPump
*
*
\************************************************************************/
require_once("../sistema/configuracion.php");
require_once("../recursos/estatico/librerias/conectorpump.php");
require_once("../recursos/estatico/librerias/idioma.php");
$idioma = idioma("../recursos/estatico/idiomas/", $IDIOMA);
$pljson = ConectorPump::obtenerJson('https://pumplive.com/stats.json');
if($pljson['ddf']['cURL obtenerJson']['http_code'] != 200) {
	array_unshift($error, array($pljson['ddf']['cURL obtenerJson']['errno'] ,$pljson['ddf']['cURL obtenerJson']['error'], $pljson['ddf']['cURL obtenerJson']['url']));
}
else {
	$stats = $pljson[0];
}
if($error) {
	$count = count($error);
	echo "<div class='mensajes'>";
	for($x=0;$x<$count;$x++) {
		$n = $error[$x][0];
		$c = $error[$x][1];
		$s = $error[$x][2];
		include("recursos/estatico/idiomas/".$idioma."/mensaje.php");
		include('recursos/estatico/esquema/mensaje.html');
	}
	echo "</div>";
}
$servidores = $stats["hosts"];
$usuarios = $stats["users"];
$aps = $stats["activityRate"];
include("../recursos/estatico/idiomas/".$idioma."/cabecera.php");
include("../recursos/estatico/esquema/cabecera.html");
$subtitulo = $cabecera[2];
include("../recursos/estatico/idiomas/".$idioma."/subcabecera.php");
include("../recursos/estatico/esquema/subcabecera.html");
include("../recursos/estatico/idiomas/".$idioma."/acerca.php");
include("../recursos/estatico/esquema/acerca.html");
include("../recursos/estatico/idiomas/".$idioma."/pie.php");
include("../recursos/estatico/esquema/pie.html");
?>