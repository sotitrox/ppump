-- Adminer 4.1.0 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `eventos`;
CREATE TABLE `eventos` (
  `sinc` datetime NOT NULL,
  `usudb` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `eventos` (`sinc`, `usudb`) VALUES
('2014-08-10 05:43:35',	'2014-08-10 05:53:52');

DROP TABLE IF EXISTS `puser`;
CREATE TABLE `puser` (
  `num` int(11) NOT NULL AUTO_INCREMENT,
  `id` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `alias` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `avatar` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `lugar` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `creado` datetime NOT NULL,
  `actualizado` datetime NOT NULL,
  `seguidores` int(11) NOT NULL,
  `url` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `acerca` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`num`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- 2014-08-10 06:02:56
