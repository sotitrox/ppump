<?php
/************************************************************************\
*
*    PPump 0.3.1 Copyright 2014 Sotitrox
*    sotitrox@autistici.org
*
*    This file is part of PPump.
*
*    PPump is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    PPump is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
*
*    Public Pump o Pump Publico es programa escrito en PHP que permite
*    interpretar el canal web publico de la red Pump.io desde el sitio
*    ofirehose.com (https://ofirehose.com/feed.json) en formato Json 
*    para su fácil lectura.
*    Para información de su uso visite:
*    http://wiki.redaustral.tk/wikka.php?wakka=PPump
*
*
\************************************************************************/
$TITULO = "Pump Público";
$PALABRASCLAVE = "pumpio, freeweb, freedom";
$CONCAR = "UTF-8";
$BASE = "http://Sitio WEB Y CARPETA DE PPUMP/";
$CONTACTO= "correo@electronico.org";
$IDIOMA = "es"; #Idioma predeterminado por si no se encuentra habilitado el idioma del visitante.
$HOSPEDAJE = "Mi compu :D"; #Donde se aloja el sitio.(se puede poner un enlace con la etiqueta "a" de html)
#B.D.
$bd_host = "HOSPEDAJE";
$bd_usuario = "USUARIO";
$bd_password = "CONTRASEÑA";
$bd_base = "BASE DE DATOS";
$con = mysql_connect($bd_host, $bd_usuario, $bd_password);
mysql_select_db($bd_base, $con);
mysql_query("SET NAMES $CONCAR");
mysql_query("SET CHARACTER_SET $CONCAR");
#filtro anti-spam
$filtro = array(
'goodview6',
'10.0.1.159',
'lovie17mj@pumpbuddy.us',
'donaldnh94@pumpbuddy.us',
'danieldtar17blog@pumpbuddy.us',
'chase1840t@pumpbuddy.us',
'ryanqublog63@pumpbuddy.us',
'larhonda417ok@pumpbuddy.us',
'ryanwp65blog@pumpbuddy.us',
'jameszc23blog@pumpbuddy.us'
);
#pseudocron, si usted cuenta con cron, por favor visitar: http://wiki.redaustral.tk/wikka.php?wakka=PPump
#1 deshabilita pseudocron
$cron = 0;
/*Tiempo en segundos
Hora	3600
Día	86400
Semana	604800
Mes	2419200
Año	29030400
*/
#intervalo actualización pseudoCron en segundos
$interact = 600;
#intervalo sincronización pseudoCron en segundos
$intersin = 86400;
#Lista de instancias PPump
$bdusuarios = array(
'https://www.inventati.org/ppump/usuarios/usuarios.php',
'http://ppump.redaustral.tk/usuarios/usuarios.php',
'http://pump.mamalibre.com.ar/usuarios/usuarios.php'
);
#cURL
$TIMEOUT = 10;
#software
$version = "0.3.1";
header('Content-type: text/html; charset=UTF-8', true );
date_default_timezone_set("UTC");
?>