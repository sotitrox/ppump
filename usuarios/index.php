<?php
/************************************************************************\
*
*    PPump 0.3.1 Copyright 2014 Sotitrox
*    sotitrox@autistici.org
*
*    This file is part of PPump.
*
*    PPump is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    PPump is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
*
*    Public Pump o Pump Publico es programa escrito en PHP que permite
*    interpretar el canal web publico de la red Pump.io desde el sitio
*    ofirehose.com (https://ofirehose.com/feed.json) en formato Json 
*    para su fácil lectura.
*    Para información de su uso visite:
*    http://wiki.redaustral.tk/wikka.php?wakka=PPump
*
*
\************************************************************************/
require_once("../sistema/configuracion.php");
require_once("../recursos/estatico/librerias/conectorpump.php");
require_once("../recursos/estatico/librerias/idioma.php");
$pljson = ConectorPump::obtenerJson('https://pumplive.com/stats.json');
if($pljson['ddf']['cURL obtenerJson']['http_code'] != 200) {
	array_unshift($error, array($pljson['ddf']['cURL obtenerJson']['errno'] ,$pljson['ddf']['cURL obtenerJson']['error'], $pljson['ddf']['cURL obtenerJson']['url']));
}
else {
	$stats = $pljson[0];
}
$idioma = idioma("../recursos/estatico/idiomas/",$IDIOMA);
$timestamp = array(
	0 => 0,
	1 => 3600,
	2 => 86400,
	3 => 604800,
	4 => 2419200,
	5 => 29030400
);
$orden = array(
	1 => 'seguidores',
	2 => 'creado',
	3 => 'alias',
	4 => 'id',
	5 => 'actualizado'
);
$crecimiento = array(
	1 => 'DESC',
	2 => 'ASC'
);
$tiempo = ConectorPump::selloTiempo();
/* Paginacion, obtiene el numero de pagina */
$cantidad = 100;
if($_GET) {
	$where = array();
	if($_GET['p']) {
		$paginacion = "LIMIT ".$_REQUEST['p'].",".$cantidad;
	}
	else {
		$paginacion = "LIMIT 0,".$cantidad;
	}
	if($_GET['o']) {
		$orden = "ORDER BY ".$orden[$_GET['o']];
	}
	else {
		$orden = "ORDER BY seguidores";
	}
	if($_GET['c']) {
		$crecimiento = $crecimiento[$_GET['c']];
	}
	else {
		$crecimiento = "DESC";
	}
	if($_GET['l']) {
		array_unshift($where, "lugar LIKE '%".$_GET['l']."%'");
	}
	if($_GET['a']) {
		array_unshift($where, "alias LIKE '%".$_GET['a']."%'");
	}
	if($_GET['s']) {
		array_unshift($where, "id LIKE '%".$_GET['s']."%'");
	}
	if($_GET['b']) {
		array_unshift($where, "acerca LIKE '%".$_GET['b']."%'");
	}
	if($_GET['f']) {
		if($_GET['f']>5) {
			$mult = $_GET['f']-4;
			$minimo = $tiempo-($timestamp[5]*$mult);
		}
		else {
		$minimo = $tiempo-$timestamp[$_GET['f']];
		}
		array_unshift($where, "creado >= '".date("Y-m-d H:i:s", $minimo)."'");
	}
	if(count($where) > 1) {
		$where = "WHERE ". implode(" AND ", $where);
	}
	else {
		$where = "WHERE ".$where[0];
	}
	if($where == "WHERE ") {
		$where = "";
	}
}
else {
	$where = "";
	$orden = "ORDER BY seguidores";
	$crecimiento = "DESC";
	$paginacion = "LIMIT 0,".$cantidad;
}
#obtener la solicitud
if(!$_SERVER['QUERY_STRING']) {
	$url=$_SERVER['PHP_HOST'].$_SERVER['PHP_SELF'];
}
else {
	$url=$_SERVER['PHP_HOST'].$_SERVER['PHP_SELF'];
	$array_solicitud = explode("&", $_SERVER['QUERY_STRING']);
	$tamanio = count($array_solicitud);
	for ($x=0;$x<$tamanio; $x++) {
		$llave_cadena = explode("=", $array_solicitud[$x]);
		unset($array_solicitud[$x]);
		$array_solicitud[$llave_cadena[0]] = $llave_cadena[1];
	}
}
function CrearUrl($array) {
	$tamanio = count($array);
	$cadena_solicitud = "";
	for ($x=0;$x<$tamanio; $x++) {
		if($x+1==$tamanio) {
			$sep = "";	
		}
		else {
			$sep = "&";	
		}
		$cadena_solictud .= key($array)."=".$array[key($array)].$sep;
		next($array);
	}
	return $cadena_solictud;
}
if($pljson['ddf']['cURL obtenerJson']['http_code'] != 200) {
	array_unshift($error, array($pljson['ddf']['cURL obtenerJson']['errno'] ,$pljson['ddf']['cURL obtenerJson']['error'], $pljson['ddf']['cURL obtenerJson']['url']));
}
else {
	$stats = $pljson[0];
}
include("../recursos/estatico/idiomas/".$idioma."/tnombres.php");
$servidores = $stats["hosts"];
$usuarios = $stats["users"];
$aps = $stats["activityRate"];
$mientras = "";
include("../recursos/estatico/idiomas/".$idioma."/cabecera.php");
include("../recursos/estatico/esquema/cabecera.html");
$subtitulo = $cabecera[1];
include("../recursos/estatico/idiomas/".$idioma."/subcabecera.php");
include("../recursos/estatico/esquema/subcabecera.html");
echo "<div class=principal>";
if($error) {
	$count = count($error);
	echo "<div class='mensajes'>";
	for($x=0;$x<$count;$x++) {
		$n = $error[$x][0];
		$c = $error[$x][1];
		$s = $error[$x][2];
		include("../recursos/estatico/idiomas/".$idioma."/mensaje.php");
		include('../recursos/estatico/esquema/mensaje.html');
	}
	echo "</div>";
}
echo "<div id='usuarios'>";
$sql = mysql_query("select id, alias, avatar, lugar, creado, actualizado, seguidores, url, acerca  from puser $where $orden $crecimiento $paginacion",$con) or die("Problemas en el select1:".mysql_error());
$impresos = 0;
while($sql_users = mysql_fetch_array($sql)) {
		$impresos++;
		$id = $sql_users['id'];
		$alias = $sql_users['alias'];
		$avatar	= $sql_users['avatar'];
		if(!$avatar) {
			$avatar	= "recursos/estatico/graficos/avatar-pre.png";
		}
		$lugar = $sql_users['lugar'];
		$seguidores = $sql_users['seguidores'];
		$url = $sql_users['url'];
		$acerca = rawurldecode($sql_users['acerca']);
		
		$fecha_pre1=date_create($sql_users['creado']);
		$diac = $dias[date_format($fecha_pre1, 'N')];
		$mesc = $meses[date_format($fecha_pre1, 'n')];
		$fecha_pre2=date_create($sql_users['actualizado']);
		$diaa = $dias[date_format($fecha_pre2, 'N')];
		$mesa = $meses[date_format($fecha_pre2, 'n')];
		foreach($filtro as $val) {
			$coincidencia = strpos($id, $val);
			if($coincidencia === false) {
				$mostrar = 1;
			}
			else {
				$mostrar = 0;
				break;
			}
		}
		if($mostrar) {
			include("../recursos/estatico/idiomas/".$idioma."/usuarios.php");
			include('../recursos/estatico/esquema/usuarios.html');
		}
}
echo "</div>";
#Paginación
	$url = $BASE."usuarios/";
	if ($_REQUEST['p']==0)
  		$paginaciona='none';
	else {
  		$array_solicitud["p"]=$_REQUEST['p']-$cantidad;
  		$paginaciona=$url."?".CrearUrl($array_solicitud);
	}
	if ($impresos==$cantidad) {
		$array_solicitud["p"]=$_REQUEST['p']+$cantidad;
  		$paginacions=$url."?".CrearUrl($array_solicitud);
	}
	else
	{
  		$paginacions='none';
	}
	if($paginaciona != '' || $paginacions != '') {
		include("../recursos/estatico/idiomas/".$idioma."/paginacion.php");
		include('../recursos/estatico/esquema/paginacion.html');
	}
echo "</div>";
echo "<div class=secundario>";
include("../recursos/estatico/idiomas/".$idioma."/usuarios-busqueda.php");
include("../recursos/estatico/esquema/usuarios-busqueda.html");
echo "</div>";
include("../recursos/estatico/idiomas/".$idioma."/pie.php");
include("../recursos/estatico/esquema/pie.html");
?>