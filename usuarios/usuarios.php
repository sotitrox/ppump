<?php
/************************************************************************\
*
*    PPump 0.3.1 Copyright 2014 Sotitrox
*    sotitrox@autistici.org
*
*    This file is part of PPump.
*
*    PPump is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    PPump is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
*
*    Public Pump o Pump Publico es programa escrito en PHP que permite
*    interpretar el canal web publico de la red Pump.io desde el sitio
*    ofirehose.com (https://ofirehose.com/feed.json) en formato Json 
*    para su fácil lectura.
*    Para información de su uso visite:
*    http://wiki.redaustral.tk/wikka.php?wakka=PPump
*
*
\************************************************************************/
require_once("../sistema/configuracion.php");
require_once("../recursos/estatico/librerias/conectorpump.php");
$timestamp = array(
	0 => 0,
	1 => 3600,
	2 => 86400,
	3 => 604800,
	4 => 2419200,
	5 => 29030400
);
$orden = array(
	1 => 'seguidores',
	2 => 'creado',
	3 => 'alias',
	4 => 'id',
	5 => 'actualizado'
);
$crecimiento = array(
	1 => 'DESC',
	2 => 'ASC'
);
$tiempo = ConectorPump::selloTiempo();
if($_GET) {
$where = array();
	if($_GET['o']) {
		$orden = "ORDER BY ".$orden[$_GET['o']];
	}
	else {
		$orden = "ORDER BY seguidores";
	}
	if($_GET['c']) {
		$crecimiento = $crecimiento[$_GET['c']];
	}
	else {
		$crecimiento = "DESC";
	}
	if($_GET['l']) {
		array_unshift($where, "lugar LIKE '%".$_GET['l']."%'");
	}
	if($_GET['a']) {
		array_unshift($where, "alias LIKE '%".$_GET['a']."%'");
	}
	if($_GET['s']) {
		array_unshift($where, "id LIKE '%".$_GET['s']."%'");
	}
	if($_GET['b']) {
		array_unshift($where, "acerca LIKE '%".$_GET['b']."%'");
	}
	if($_GET['f']) {
		if($_GET['f']>5) {
			$mult = $_GET['f']-4;
			$minimo = $tiempo-($timestamp[5]*$mult);
		}
		else {
		$minimo = $tiempo-$timestamp[$_GET['f']];
		}
		array_unshift($where, "creado >= '".date("Y-m-d H:i:s", $minimo)."'");
	}
	if(count($where) > 1) {
		$where = "WHERE ". implode(" AND ", $where);
	}
	else {
		$where = "WHERE ".$where[0];
	}
	if($where == "WHERE ") {
		$where = "";
	}
}
else {
	$where = "";
	$orden = "ORDER BY id";
	$crecimiento = "DESC";
}
$dias = array(
1 => "Lunes",
2 => "Martes",
3 => "Miércoles",
4 => "Jueves",
5 => "Viernes",
6 => "Sábado",
7 => "Domingo"
);
$meses = array(
1 => "Enero",
2 => "Febrero",
3 => "Marzo",
4 => "Abril",
5 => "Mayo",
6 => "Junio",
7 => "Julio",
8 => "Agosto",
9 => "Septiembre",
10 => "Octubre",
11 => "Noviembre",
12 => "Diciembre"
);
$usuarios = array();
$sql = mysql_query("select id, alias, avatar, lugar, creado, actualizado, seguidores, url, acerca  from puser $where $orden $crecimiento",$con) or die("Problemas en el select1:".mysql_error());
while($sql_users = mysql_fetch_array($sql)) {
	$usuario = array(
		'id' => $sql_users['id'],
		'alias' => $sql_users['alias'],
		'avatar'	=> $sql_users['avatar'],
		'lugar' => $sql_users['lugar'],
		'seguidores' => $sql_users['seguidores'],
		'url' => $sql_users['url'],
		'acerca' => $sql_users['acerca'],
		'creado' => $sql_users['creado'],
		'actualizado' => $sql_users['actualizado']
		);
	array_unshift($usuarios, $usuario);
}
header('Content-type: text/json; charset=UTF-8', true );
echo json_encode($usuarios);
?>