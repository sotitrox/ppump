<?php
/************************************************************************\
*
*    PPump 0.3.1 Copyright 2014 Sotitrox
*    sotitrox@autistici.org
*
*    This file is part of PPump.
*
*    PPump is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    PPump is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
*
*    Public Pump o Pump Publico es programa escrito en PHP que permite
*    interpretar el canal web publico de la red Pump.io desde el sitio
*    ofirehose.com (https://ofirehose.com/feed.json) en formato Json 
*    para su fácil lectura.
*    Para información de su uso visite:
*    http://wiki.redaustral.tk/wikka.php?wakka=PPump
*
*
\************************************************************************/
require_once("sistema/configuracion.php");
require_once("recursos/estatico/librerias/conectorpump.php");
require_once("recursos/estatico/librerias/idioma.php");
$idioma = idioma("recursos/estatico/idiomas/", $IDIOMA);
$error = array();
$ojson = ConectorPump::obtenerJson('https://ofirehose.com/feed.json');
if($ojson['ddf']['cURL obtenerJson']['http_code'] != 200) {
	array_unshift($error, array($ojson['ddf']['cURL obtenerJson']['errno'] ,$ojson['ddf']['cURL obtenerJson']['error'], $ojson['ddf']['cURL obtenerJson']['url']));
}
else {
	$response = $ojson[0];
}
$pljson = ConectorPump::obtenerJson('https://pumplive.com/stats.json');
if($pljson['ddf']['cURL obtenerJson']['http_code'] != 200) {
	array_unshift($error, array($pljson['ddf']['cURL obtenerJson']['errno'] ,$pljson['ddf']['cURL obtenerJson']['error'], $pljson['ddf']['cURL obtenerJson']['url']));
}
else {
	$stats = $pljson[0];
}
if(!$cron) {
	require_once("recursos/estatico/librerias/sincronizar.php");
	require_once("recursos/estatico/librerias/actualizar.php");
	require_once("recursos/estatico/librerias/pseudocron.php");
	if($response) {
		pseudoCron(array('actualizarBD', array($response, $filtro, $con)), $interact, "usudb", $con);
		pseudoCron(array('sincronizarBD', array($bdusuarios, $con)), $intersin, "sinc", $con);
	}
}
include("recursos/estatico/idiomas/".$idioma."/tnombres.php");
$titulo_feed = $response ['displayName'];
$servidores = $stats["hosts"];
$usuarios = $stats["users"];
$aps = $stats["activityRate"];
$mientras = "";
include("recursos/estatico/idiomas/".$idioma."/cabecera.php");
include("recursos/estatico/esquema/cabecera.html");
$subtitulo = $cabecera[0];
include("recursos/estatico/idiomas/".$idioma."/subcabecera.php");
include("recursos/estatico/esquema/subcabecera.html");
echo "<div class=principal>";
if($error) {
	$count = count($error);
	echo "<div class='mensajes'>";
	for($x=0;$x<$count;$x++) {
		$n = $error[$x][0];
		$c = $error[$x][1];
		$s = $error[$x][2];
		include("recursos/estatico/idiomas/".$idioma."/mensaje.php");
		include('recursos/estatico/esquema/mensaje.html');
	}
	echo "</div>";
}
echo "<div id='entradas'>";
if($_GET['filtro']) {
	$filtro = array_merge($filtro, explode("%2C", rawurlencode($_GET['filtro'])));
}
$tamanio = count($response['items']);
for ($x=0;$x<$tamanio; $x++) {
	$mientras.=$response['items']["$x"]['content']."<br>";
	foreach($filtro as $val) {
		$coincidencia = strpos(rawurlencode(str_replace("acct:", "", $response['items']["$x"]['actor']['id'])), $val);
		if($coincidencia === false) {
			if($response['items'][$x-1]['verb'] == 'update' && $response['items'][$x]['object']['id'] == $response['items'][$x-1]['object']['id']) {					
				$mostrar = 0;
				break;
			}
			else {
				$mostrar = 1;
			}
		}
		else {
			$mostrar = 0;
			break;
		}
	}
	if(($response['items']["$x"]['verb']=='post' || $response['items']["$x"]['verb']=='update') && $response['items']["$x"]['object']['objectType'] != 'comment' && $mostrar) {
		$avatar=$response['items']["$x"]['actor']['image']['url'];
		if(!$avatar) {
			$avatar = "noavatar";
		}
		$nombre=$response['items']["$x"]['actor']['displayName'];
		$usuario=str_replace("acct:", "", $response['items']["$x"]['actor']['id']);
		$alias = $response['items']["$x"]['actor']['id'];
		$seguidores=$response['items']["$x"]['actor']['followers']['totalItems'];
		$url_usuario=$response['items']["$x"]['actor']['url'];
		$mensaje=$response['items']["$x"]['object']['content'];
		$fecha_pre=date_create($response['items']["$x"]['object']['published']);
		$dia = $dias[date_format($fecha_pre, 'N')];
		$mes = $meses[date_format($fecha_pre, 'n')];
		$url=$response['items']["$x"]['object']['url'];
		$generador=$response['items']["$x"]['generator']['displayName'];
		if($response['items']["$x"]['object']['displayName']) {
			$titulo = "<h1>".$response['items']["$x"]['object']['displayName']."</h1><br>";
		}
		else {
			$titulo = "";
		}
		if($response['items']["$x"]['object']['objectType'] == 'note') {
			include("recursos/estatico/idiomas/".$idioma."/post-nota.php");
			include("recursos/estatico/esquema/post-nota.html");
		}
		elseif($response['items']["$x"]['object']['objectType'] == 'image') {
			$imagen = $response['items']["$x"]['object']['fullImage']['url'];
			include("recursos/estatico/idiomas/".$idioma."/post-image.php");
			include("recursos/estatico/esquema/post-imagen.html");
		}
	}
	elseif(($response['items']["$x"]['verb']=='post' || $response['items']["$x"]['verb']=='update') && $response['items']["$x"]['object']['objectType'] == 'comment' && $mostrar) {
		#autor comentario
		$comava=$response['items']["$x"]['actor']['image']['url'];
		if(!$comava) {
			$comava = "noavatar";
		}
		$comnom=$response['items']["$x"]['actor']['displayName'];
		$comusuid=str_replace("acct:", "", $response['items']["$x"]['actor']['id']);
		$comalias = $response['items']["$x"]['actor']['id'];
		$comseg=$response['items']["$x"]['actor']['followers']['totalItems'];
		$comusuurl=$response['items']["$x"]['actor']['url'];
		$commen=$response['items']["$x"]['object']['content'];
		$comfecha_pre=date_create($response['items']["$x"]['object']['published']);
		$comdia = $dias[date_format($comfecha_pre, 'N')];
		$commes = $meses[date_format($comfecha_pre, 'n')];
		$comurl=$response['items']["$x"]['object']['url'];
		$comgen=$response['items']["$x"]['generator']['displayName'];
		#autor nota
		$avatar=$response['items']["$x"]['object']['inReplyTo']['author']['image']['url'];
		if(!$avatar) {
			$avatar = "noavatar";
		}
		$nombre=$response['items']["$x"]['object']['inReplyTo']['author']['displayName'];
		$usuario=str_replace("acct:", "", $response['items']["$x"]['object']['inReplyTo']['author']['id']);
		$alias = $response['items']["$x"]['object']['inReplyTo']['author']['id'];
		$seguidores=$response['items']["$x"]['object']['inReplyTo']['author']['followers']['totalItems'];
		$url_usuario=$response['items']["$x"]['object']['inReplyTo']['author']['url'];
		$mensaje=$response['items']["$x"]['object']['inReplyTo']['content'];
		$fecha_pre=date_create($response['items']["$x"]['object']['inReplyTo']['published']);
		$dia = $dias[date_format($fecha_pre, 'N')];
		$mes = $meses[date_format($fecha_pre, 'n')];
		$url=$response['items']["$x"]['object']['inReplyTo']['url'];
		$generador=$response['items']["$x"]['object']['inReplyTo']['generator']['displayName'];
		if($response['items']["$x"]['object']['inReplyTo']['displayName']) {
			$titulo = $response['items']["$x"]['object']['inReplyTo']['displayName'];
		}
		else {
			$titulo = "";
		}
		if($response['items']["$x"]['object']['inReplyTo']['objectType'] == 'note') {
			include("recursos/estatico/idiomas/".$idioma."/post-comentario.php");
			include("recursos/estatico/esquema/post-nota-comentario.html");
		}
		elseif($response['items']["$x"]['object']['inReplyTo']['objectType'] == 'image') {
			include("recursos/estatico/idiomas/".$idioma."/post-comentario-imagen.php");
			$imagen = $response['items']["$x"]['object']['inReplyTo']['fullImage']['url'];
			include("recursos/estatico/esquema/post-imagen-comentario.html");
		}
	}
	elseif(($response['items']["$x"]['verb']=='like' || $response['items']["$x"]['verb']=='favorite') && $mostrar) {
		#usuario
		$avatar=$response['items']["$x"]['actor']['image']['url'];
		if(!$avatar) {
			$avatar = "noavatar";
		}
		$nombre=$response['items']["$x"]['actor']['displayName'];
		$pumpid=str_replace("acct:", "", $response['items']["$x"]['actor']['id']);
		$seguidores=$response['items']["$x"]['actor']['followers']['totalItems'];
		$url=$response['items']["$x"]['actor']['url'];
		$fecha_pre=date_create($response['items']["$x"]['object']['published']);
		$dia = $dias[date_format($fecha_pre, 'N')];
		$mes = $meses[date_format($fecha_pre, 'n')];
		#objeto
		$comurl=$response['items']["$x"]['object']['url'];
		$generador=$response['items']["$x"]['generator']['displayName'];
		$objautornombre = $response['items']["$x"]['object']['author']['displayName'];
		$objautorid = str_replace("acct:", "", $response['items']["$x"]['object']['author']['id']);
		$objautorurl = $response['items']["$x"]['object']['author']['url'];
		$urlactividad = $response['items']["$x"]['url'];
		if($response['items']["$x"]['object']['displayName']) {
			$titulo = $response['items']["$x"]['object']['displayName'];
		}
		else {
			$titulo = $response['items']["$x"]['object']['objectType'];
		}
		include("recursos/estatico/idiomas/".$idioma."/like.php");
		include("recursos/estatico/esquema/like.html");
	}
}
echo "</div>";
echo "</div>";
echo "<div class=secundario>";
include("recursos/estatico/idiomas/".$idioma."/mientrastanto.php");
include("recursos/estatico/esquema/mientrastanto.html");
echo "</div>";
include("recursos/estatico/idiomas/".$idioma."/pie.php");
include("recursos/estatico/esquema/pie.html");
#echo "<pre>";
#print_r($actualizarBD);
#print_r($response);
#print_r($stats);
#print_r($data);
#echo "</pre>";
?>