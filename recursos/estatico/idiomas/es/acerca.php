<?php
$acerca = array(
0 => $TITULO.' es el interprete del canal público de la red Pump.io (ofirehose.com), para su fácil lectura, además las dispone vía web o vía canal web en formato RSS. Pump Público cuenta además con filtros de spam, una base de datos publica de usuarios, próximamente mas utilidades serán añadidas, con el fin abrir pump.io al mundo y facilitar la entrada a los usuarios nuevos.',
1 => $TITULO.' utiliza el software PPump escrito en PHP bajo licencia GPL v3, y la apariencia del sitio moldeada con HTML y personalizada con CSS.',
2 => 'Funcionamiento',
3 => 'PPump obtiene los principales datos del sitio <a href="https://ohirehose.com">Ofirehose</a>, de ahí obtiene información periódicamente para completar el directorio público de usuarios. También hace uso de otros servicios, tal como <a href="https://pump2rss.com">pump2rss</a>, <a href="https://www.pumplive.com/">Pump live</a> y <a href="https://www.openstreetmap.org">OpenStreetMap</a>.',
4 => 'Canal Web RSS',
5 => $TITULO.' cuenta con un canal web en formato RSS, los datos que entrega ofirehose.com no tienen filtro anti-spam, así que el canal web cuenta con filtros de usuarios, es decir, usted quita los <i>posts</i> de usuarios que en su id de Pump.io contengan la cadena de texto especificada en la variable <i>filtro</i>, ejemplo:',
6 => 'En la siguiente url, el canal web mostrara todos los post, a excepción  de los post del usuario que tiene como alias en su id de pump.io la palabra <i>protestation</i>, los posts de usuarios que provienen de <i>identi.ca</i> y los post del usuario <i>sotitrox@i.rationa.li</i>:',
7 => 'Note que en el ejemplo mostramos como filtrar de tres formas, y las tres formas son perfectamente combinables, lo único que debe saber es que debe escribir alguna palabra, sección de texto, letra, numero o cualquier carácter que este permitido en la id de Pump.io de los usuarios que quiera quitar y no se mostraran en su canal web.',
8 => 'Enlaces',
9 => 'Código de Fuente',
10 => 'Reportes de Fallos'
);
?>