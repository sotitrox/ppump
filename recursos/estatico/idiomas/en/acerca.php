<?php
$acerca = array(
0 => $TITULO.' is the interpreter of the Pump.io network\'s public feed  (ofirehose.com) for easy reading, also show this via web browser or via feed RSS. Public Pump also have spam filters and a public users database, soon will be added new utilities, the objetive is open pump.io to the world and help to new users start in the pump.io network.',
1 => $TITULO.' uses free software written in PHP, PPump, and the appeareance of site was written in HTML and personaliced with CSS.',
2 => 'How the system works',
3 => 'PPump get major site data <a href="https://ohirehose.com"> Ofirehose </a>, hence obtains information periodically to complete the public directory of users. It also makes use of other services, such as <a href="https://pump2rss.com">pump2rss</a>, <a href="https://www.pumplive.com/">Pump live</a> and <a href="https://www.openstreetmap.org">OpenStreetMap</a>.',
4 => 'RSS Feed',
5 => $TITULO.' has a RSS feed, ofirehose.com don\'t have anti-spam filter, so we have one, you can remove the posts of users that have in the Pump.io id the substring that is defined in the variable <i>filtro</i>, eg:',
6 => 'In the following URL, the feed will show every post except the post of the user that have the alias <i>protestation</i> in the pump id, the posts of users are from <i>identi.ca</i> and posts of the user <i>sotitrox@i.rationa.li</i>:',
7 => 'Note that in the example we show how to use the filter in three ways, and three ways are perfectly combined, the only thing you should know is that you must enter a word, substring, letter, number (or any character allowed in the Pump.io id) of users who want to remove and will not be displayed on your feed.',
8 => 'Links',
9 => 'Source Code',
10 => 'Buglist'
);
?>