<?php
/************************************************************************\
*
*    PPump 0.3.1 Copyright 2014 Sotitrox
*    sotitrox@autistici.org
*
*    This file is part of PPump.
*
*    PPump is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    PPump is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
*
*    Public Pump o Pump Publico es programa escrito en PHP que permite
*    interpretar el canal web publico de la red Pump.io desde el sitio
*    ofirehose.com (https://ofirehose.com/feed.json) en formato Json 
*    para su fácil lectura.
*    Para información de su uso visite:
*    http://wiki.redaustral.tk/wikka.php?wakka=PPump
*
*
\************************************************************************/
class ConectorPump {
	public static $userAgent = 'Granada Bot - http://granada.redaustral.tk/acerca.php';
	public static $rutaURL = array(
		'tt' => '/oauth/request_token',
		'ta' => '/oauth/access_token',
		'rc' => '/api/client/register'
	);
	public static $metodoCod = 'HMAC-SHA1';
	function rutasGet($usuario, $cantidad) {
		if($cantidad) {
			$argumento = '?';
			$argumentos = array();
			if($cantidad) {
				array_unshift($argumentos, 'count='.$cantidad);		
			}
			$arg = '?'.implode('&', $argumentos);
		}
		$rutas = array(
			'outbox' => '/api/user/'.$usuario.'/feed'.$arg,
			'outbox minor' => '/api/user/'.$usuario.'/feed/minor'.$arg,
			'outbox major' => '/api/user/'.$usuario.'/feed/major'.$arg,
			'inbox' => '/api/user/'.$usuario.'/inbox'.$arg,
			'inbox minor' => '/api/user/'.$usuario.'/inbox/minor'.$arg,
			'inbox major' => '/api/user/'.$usuario.'/inbox/major'.$arg,
			'inbox direct' => '/api/user/'.$usuario.'/inbox/direct'.$arg,
			'inbox direct minor' => '/api/user/'.$usuario.'/inbox/direct/minor'.$arg,
			'inbox direct major' => '/api/user/'.$usuario.'/inbox/direct/major'.$arg,
			'followers' => '/api/user/'.$usuario.'/followers',
			'following' => '/api/user/'.$usuario.'/following',
			'favorites' => '/api/user/'.$usuario.'/favorites',
			'lists' => '/api/user/'.$usuario.'/lists',
			'profile' => '/api/user/'.$usuario.'/profile'
		);
		return $rutas;
	}
	function cURL($URL, $metodo, $cabecera_extra, $post) {
		$ch = curl_init($URL);
		$opciones = array(
			CURLOPT_SSL_VERIFYPEER => false,
   		CURLOPT_RETURNTRANSFER => true,
   		CURLOPT_USERAGENT => self::$userAgent,
   		CURLINFO_HEADER_OUT => true,
   		CURLOPT_FOLLOWLOCATION => false,
   		CURLOPT_CONNECTTIMEOUT => 10,
   		CURLOPT_HTTPHEADER => array(
   		)
		);
		if($post) {
			$opciones[CURLOPT_POSTFIELDS] = $post;
		}
		if($metodo === "POST") {
			$opciones[CURLOPT_POST] = true;
		}
		if($cabecera_extra) {
			$opciones[CURLOPT_HTTPHEADER] = array_merge($opciones[CURLOPT_HTTPHEADER], $cabecera_extra);
		}
		curl_setopt_array($ch, $opciones);
		$exec = curl_exec($ch);
		$curl_info = curl_getinfo($ch);
		$curl_info['error'] = curl_error($ch);
		$curl_info['errno'] = curl_errno($ch);
		if($exec){
   		$respuesta = array($exec, $curl_info);
		}
		else {
			$respuesta = array(0, $curl_info);
		}
		curl_close($ch);
		return $respuesta;
	}
	function registrarCliente($host) {
		$URL = 'https://' . $host . self::$rutaURL['rc'];
		$metodo = 'POST';
		$cabecera_extra = array('Accept: application/json', 'Content-Type: application/json');
		$post = json_encode(self::$parametrosCliente);
		$registro = self::cURL($URL, $metodo, $cabecera_extra, $post);
		if(!$registro[0]){
			#si la conexion segura falla, se prueba con la conexión común
			$URL = 'http://' . $host . self::$rutaURL['rc'];
			$registro_inseguro = self::cURL($URL, $metodo, $cabecera_extra, $post);
			#si aun asi la conexión falla entonces imprime un mensaje de error en pantalla, se debe redirigir a otro sitio!!!!
			if(!$registro_inseguro[0]){
				return array(0, 'ddf' => array('cURL registrarCliente protocolo seguro' => $registro[1], 'cURL registrarCliente protocolo inseguro' => $registro_inseguro[1]));
			}
			else {
				$conexion = '';
				$credenciales = json_decode($registro_inseguro[0], true);
				$ddf = array('cURL registrarCliente protocolo seguro' => $registro[1], 'cURL registrarCliente protocolo inseguro' => $registro_inseguro[1]);
			}
		}
		else {
			$conexion = 's';
			$credenciales = json_decode($registro[0], true);
			$ddf = array('cURL registrarCliente protocolo seguro' => $registro[1]);
		}
		$oauth_consumer_key = $credenciales["client_id"];
		$oauth_consumer_secret = $credenciales["client_secret"];
		$resultado = array(
		'conexion' => $conexion,
		'consumer_key' => $oauth_consumer_key,
		'consumer_secret' => $oauth_consumer_secret,
		'ddf' => $ddf
		);
		return $resultado;
	}
	function fabricarEnlace($conexion, $host, $camino) {
		$enlace = 'http'.$conexion.'://'.strtolower($host).$camino;
		return $enlace;
	}
	function selloTiempo() {
		$php=phpversion();
		$php=substr($php,0,3);
		if ($php >= "5.3") {
			$fecha = date_create();
			$segundos = date_timestamp_get($fecha);
		}
		else {
			$fecha = new DateTime();
			$segundos = $fecha->format("U");
		}
		return $segundos;
	}
	function nonce($variable) {
		$nonce = hash('md5', $variable);
		return $nonce;
	}
	function authorizationOauth($publico, $secreto, $metodo, $enlace) {
		$parametros = "";
		$authorization = "";
		#buscamos, si es que los hay, argumentos extras en al url y las agregamos a las variables para la firma.
		$argumentosURL = array();
		$query = parse_url($enlace, PHP_URL_QUERY);
		$enlace = str_replace('?'.$query, '', $enlace);#quitamos los argumentos y el simbolo "?"
		parse_str($query, $argumentosURL);
		$a_param = array_merge($publico, $argumentosURL);
		ksort($a_param);
		foreach ($a_param as $c => $v) {
			$parametros.='&'.$c.'='.rawurlencode($v);
		}
		$parametros = substr($parametros, 1, strlen($parametros));#quita el ampersand del principio de la cadena
		#Inicio codificación firma
		$parametros = rawurlencode($parametros);
		$base_firma = $metodo.'&'.rawurlencode($enlace).'&'.$parametros;
		$llave_cod = rawurlencode($secreto["ocs"]).'&'.rawurlencode($secreto["ots"]);
		$firma = hash_hmac('SHA1',$base_firma,$llave_cod,true);
		$firma = base64_encode($firma);
		#fin codificacion firma
		#Puede reemplazar las dos lineas de abajo $publico['oauth_signature'] = $firma;
		$firma = array('oauth_signature' => $firma);
		$publico = array_merge($publico, $firma);
		ksort($publico);
		foreach ($publico as $c => $v) {
			$authorization.=','.$c.'="'.rawurlencode($v).'"';
		}
		$authorization = substr($authorization, 1, strlen($authorization));#quita la primera coma
		return $authorization;
	}
	function oauthCurlpost($url, $authorization, $post) {
		$cabecera = array('Authorization: OAuth '.$authorization);
		$response = self::cURL($url, "POST", $cabecera, $post);
		return $response;
	}
	function oauthCurlget($url, $authorization, $post) {
		$cabecera = array('Authorization: OAuth '.$authorization);
		$response = self::cURL($url, "GET", $cabecera, $post);
		return $response;
	}
	function extractorPumpid($id, $peticion) {
		if(filter_var($id, FILTER_VALIDATE_EMAIL)) {
			if($peticion == "nodo") {
				$host = substr($id, stripos($id,'@')+1, strlen($id));
				return $host;
			}
			elseif($peticion == "alias") {
				$alias = substr($id, stripos($id,'0'),stripos($id,'@'));
				return $alias;
			}
			else {
				return FALSE;
			}
		}
		else {
			return FALSE;
		}
	}
	function solicitarTokentmp($oauth_consumer_key, $oauth_consumer_secret, $conexion, $host) {
		$metodo_http = 'POST';
		$peticion_token = self::$rutaURL['tt'];
		$segundos = self::selloTiempo();
		$enlace = self::fabricarEnlace($conexion, $host, $peticion_token);
		$nonce = self::nonce($segundos);
		$callback = self::$parametrosCliente['redirect_uris'];
		$metodo_cod = self::$metodoCod;
		$publico = array(
			'oauth_callback' => $callback,
			'oauth_consumer_key' => $oauth_consumer_key,
			'oauth_nonce' => $nonce,
			'oauth_signature_method' => $metodo_cod,
			'oauth_timestamp' => $segundos,
			'oauth_version' => '1.0'
		);
		$secreto = array(
			'ocs' => $oauth_consumer_secret,
			'ots' => ''
		);
		$authorization = self::authorizationOauth($publico, $secreto, $metodo_http, $enlace);
		$cuerpo = "";
		$tokentmp_post = self::oauthCurlpost($enlace, $authorization, $cuerpo);
		if(!substr_count($tokentmp_post[0], 'oauth_token_secret')) {
			$authorization = self::authorizationOauth($publico, $secreto, 'GET', $enlace);
			$tokentmp_get = self::oauthCurlget($enlace, $authorization, $cuerpo);
			if(!substr_count($tokentmp_get[0], 'oauth_token_secret')) {
				return array($tokentmp_get[0], 'ddf' => array('cURL solicitarTokentmp POST' => $tokentmp_post[1], 'cURL solicitarTokentmp GET' => $tokentmp_get[1]));
			}
			else {
				$parse_url = 'http://siteweb.com/lol?'.$tokentmp_get[0];
				$query = parse_url($parse_url, PHP_URL_QUERY);
				$credenciales_tmp = array();
				parse_str($query, $credenciales_tmp);
				$credenciales_tmp['ddf'] = array('cURL solicitarTokentmp POST' => $tokentmp_post[1], 'cURL solicitarTokentmp GET' => $tokentmp_get[1]);
				return $credenciales_tmp;
			}
		}
		else {
			$parse_url = 'http://siteweb.com/lol?'.$tokentmp_post[0];
			$query = parse_url($parse_url, PHP_URL_QUERY);
			$credenciales_tmp = array();
			parse_str($query, $credenciales_tmp);
			$credenciales_tmp['ddf'] = array('cURL solicitarTokentmp' => $response[1]);
			return $credenciales_tmp;
		}
	}
	function solicitarTokenacc($oauth_consumer_key, $oauth_consumer_secret, $oauth_token, $oauth_token_secret, $oauth_verifier, $conexion, $host) {
		$metodo_http = 'POST';
		$peticion_token = self::$rutaURL['ta'];
		$segundos = self::selloTiempo();
		$enlace = self::fabricarEnlace($conexion, $host, $peticion_token);
		$nonce = self::nonce($segundos);
		$callback = self::$parametrosCliente['redirect_uris'];
		$metodo_cod = self::$metodoCod;
		$publico = array(
			'oauth_callback' => $callback,
			'oauth_consumer_key' => $oauth_consumer_key,
			'oauth_nonce' => $nonce,
			'oauth_signature_method' => $metodo_cod,
			'oauth_timestamp' => $segundos,
			'oauth_token' => $oauth_token,
			'oauth_verifier' => $oauth_verifier,
			'oauth_version' => '1.0'
		);
		$secreto = array(
			'ocs' => $oauth_consumer_secret,
			'ots' => $oauth_token_secret
		);
		$authorization = self::authorizationOauth($publico, $secreto, $metodo_http, $enlace);
		$cuerpo = "";
		$tokenacc_post = self::oauthCurlpost($enlace, $authorization, $cuerpo);
		if(!substr_count($tokenacc_post[0], 'oauth_token')) {
			$authorization = self::authorizationOauth($publico, $secreto, 'GET', $enlace);
			$tokenacc_get = self::oauthCurlget($enlace, $authorization, $cuerpo);
			if(!substr_count($tokenacc_get[0], 'oauth_token')) {
				return array($tokenacc_get[0], 'ddf' => array('cURL solicitarTokenacc POST' => $tokenacc_post[1], 'cURL solicitarTokenacc GET' => $tokenacc_get[1]));
			}
			else {
				$parse_url = 'http://siteweb.com/lol?'.$tokenacc_get[0];
				$query = parse_url($parse_url, PHP_URL_QUERY);
				$credenciales_acc = array();
				parse_str($query, $credenciales_acc);
				$credenciales_acc['ddf'] = array('cURL solicitarTokenacc POST' => $tokenacc_post[1], 'cURL solicitarTokenacc GET' => $tokenacc_get[1]);
				return $credenciales_acc;
			}
		}
		else {
			$parse_url = 'http://siteweb.com/lol?'.$tokenacc_post[0];
			$query = parse_url($parse_url, PHP_URL_QUERY);
			$credenciales_acc = array();
			parse_str($query, $credenciales_acc);
			$credenciales_acc['ddf'] = array('cURL solicitarTokentmp' => $response[1]);
			return $credenciales_acc;
		}
		
	}
/*	function enviarNota($posteo, $oauth_consumer_key, $oauth_consumer_secret, $oauth_token, $oauth_token_secret, $conexion, $host, $nombre) {
		$metodo_http = 'POST';
		$envio_datos = '/api/user/'.$nombre.'/feed';
		$segundos = self::selloTiempo();
		$enlace = self::fabricarEnlace($conexion, $host, $envio_datos);
		$enlace_cod = rawurlencode($enlace);
		$nonce = self::nonce($segundos);
		$metodo_cod = self::$metodoCod;
		$publico = array(
			'oauth_callback' => $callback,
			'oauth_consumer_key' => $oauth_consumer_key,
			'oauth_nonce' => $nonce,
			'oauth_signature_method' => $metodo_cod,
			'oauth_timestamp' => $segundos,
			'oauth_token' => $oauth_token,
			'oauth_version' => '1.0'
		);
		$secreto = array(
			'ocs' => $oauth_consumer_secret,
			'ots' => $oauth_token_secret
		);
		$authorization = self::authorizationOauth($publico, $secreto, $metodo_http, $enlace);
		$response = self::oauthCurlpost($enlace, $authorization, $posteo);
		return json_decode($response[0], true);
	}*/
	function obtenerDatosusuario($usuario, $conexion, $oauth_consumer_key, $oauth_consumer_secret, $oauth_token, $oauth_token_secret, $peticion, $cantidad) {
		$host = ConectorPump::extractorPumpid($usuario, "nodo");
		$alias = ConectorPump::extractorPumpid($usuario, "alias");
		$ruta = self::rutasGet($alias, $cantidad);
		$ruta = $ruta[$peticion];
		$enlace = 'http'.$conexion.'://'.$host. $ruta;
		$metodo_http = 'GET';
		$segundos = self::selloTiempo();
		$nonce = self::nonce($segundos);
		$metodo_cod = self::$metodoCod;
		$publico = array(
			'oauth_consumer_key' => $oauth_consumer_key,
			'oauth_nonce' => $nonce,
			'oauth_signature_method' => $metodo_cod,
			'oauth_timestamp' => $segundos,
			'oauth_token' => $oauth_token,
			'oauth_version' => '1.0'
		);
		$secreto = array(
			'ocs' => $oauth_consumer_secret,
			'ots' => $oauth_token_secret
		);
		$authorization = self::authorizationOauth($publico, $secreto, $metodo_http, $enlace);
		$cuerpo = "";
		$response = self::oauthCurlget($enlace, $authorization, $cuerpo);
		if($response[1]['http_code'] == 200) {
			return array(json_decode($response[0], true), 'ddf' => array('cURL obtenerDatos' => $response[1]));
		}
		else {
			return array($response[0], 'ddf' => array('cURL obtenerDatos' => $response[1]));
		}	
	}
	function obtenerJson($URL) {
		$curl = self::cURL($URL, 'GET', 0, 0);
		return array(json_decode($curl[0], true), 'ddf' => array('cURL obtenerJson' => $curl[1]));
	}
	function obtenerJson_esp($URL) {
		$curl = self::cURL($URL, 'GET', 0, 0);
		return array(json_decode(strip_tags($curl[0]), true), 'ddf' => array('cURL obtenerJson' => $curl[1]));
	}
}
?>