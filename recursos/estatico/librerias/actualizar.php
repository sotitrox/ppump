<?php
/************************************************************************\
*
*    PPump 0.3.1 Copyright 2014 Sotitrox
*    sotitrox@autistici.org
*
*    This file is part of PPump.
*
*    PPump is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    PPump is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
*
*    Public Pump o Pump Publico es programa escrito en PHP que permite
*    interpretar el canal web publico de la red Pump.io desde el sitio
*    ofirehose.com (https://ofirehose.com/feed.json) en formato Json 
*    para su fácil lectura.
*    Para información de su uso visite:
*    http://wiki.redaustral.tk/wikka.php?wakka=PPump
*
*
\************************************************************************/
function actualizarBD($response, $filtro, $con) {
	$log = array();
	$usuarios_db = array();
	$ids_db = array();
	$sql = mysql_query("select id, alias, avatar, lugar, creado, actualizado, seguidores, url, acerca from puser",$con) or die("Problemas en el select:".mysql_error());
	while($sql_users = mysql_fetch_array($sql)) {
		$a_sql_user = array(
			'id' => $sql_users['id'],
			'alias'	=> $sql_users['alias'],
			'avatar'	=> $sql_users['avatar'],
			'lugar' => $sql_users['lugar'],
			'creado' => $sql_users['creado'],
			'actualizado' => $sql_users['actualizado'],
			'seguidores' => $sql_users['seguidores'],
			'url' => $sql_users['url'],
			'acerca' => $sql_users['acerca']
		);
		array_unshift($usuarios_db, $a_sql_user);
		array_unshift($ids_db, $a_sql_user['id']);
	}	
	$usuarios = array();
	$ids = array();
	$tamanio = count($response['items']);
	for ($x=0;$x<$tamanio; $x++) {
		$array_usuario = array(
			'id' => str_replace("acct:", "", $response['items']["$x"]['actor']['id']),
			'alias'	=> $response['items']["$x"]['actor']['displayName'],
			'avatar'	=> $response['items']["$x"]['actor']['image']['url'],
			'lugar' => $response['items']["$x"]['actor']['location']['displayName'],
			'creado' => $response['items']["$x"]['actor']['_created'],
			'actualizado' => $response['items']["$x"]['actor']['updated'],
			'seguidores' => $response['items']["$x"]['actor']['followers']['totalItems'],
			'url' => $response['items']["$x"]['actor']['url'],
			'acerca' => rawurlencode($response['items']["$x"]['actor']['summary'])
		);
		if(!$array_usuario['creado']) {
			$array_usuario['creado']=$response['items']["$x"]['actor']['published'];
		}	
		if(array_search($array_usuario['id'], $ids) === false) {
			foreach($filtro as $val) {
				$coincidencia = strpos($array_usuario['id'], $val);
				if($coincidencia === false) {
					$mostrar = 1;
				}
				else {
					$mostrar = 0;
					break;
				}
			}
			if($mostrar == 1) {
				array_unshift($usuarios, $array_usuario);
				array_unshift($ids, $array_usuario['id']);
				if(array_search($array_usuario['id'], $ids_db) === false) {
					$sql = "INSERT INTO puser (id, alias, avatar, lugar, creado, actualizado, seguidores, url, acerca) ";
					$sql.= "VALUES ('".$array_usuario['id']."','".$array_usuario['alias']."','".$array_usuario['avatar']."','".$array_usuario['lugar']."','".$array_usuario['creado']."','".$array_usuario['actualizado']."','".$array_usuario['seguidores']."','".$array_usuario['url']."','".$array_usuario['acerca']."')";
					$insertar = mysql_query($sql);
					if($insertar) {
						array_unshift($log, "se agregó ".$array_usuario['id']);
					}
					else {
						array_unshift($log, "error al agregar a ".$array_usuario['id']." a la base de datos".mysql_error());
					}
				}
				else {
					$k = array_search($array_usuario['id'], $ids_db);
					$diferencia = array_diff($array_usuario, $usuarios_db[$k]);
					if(count($diferencia) > 0) {
						$sql = "UPDATE puser SET id='".$array_usuario['id']."', alias='".$array_usuario['alias']."', avatar='".$array_usuario['avatar']."', lugar='".$array_usuario['lugar']."', creado='".$array_usuario['creado']."', actualizado='".$array_usuario['actualizado']."', seguidores='".$array_usuario['seguidores']."', url='".$array_usuario['url']."', acerca='".$array_usuario['acerca']."' ";
						$sql.= "WHERE id='".$array_usuario['id']."'";
						$actualizar = mysql_query($sql);
						if($actualizar) {
							array_unshift($log, $array_usuario['id']." actualizado");
						}
						else {
							array_unshift($log, "error al actualizar a ".$array_usuario['id']." a la base de datos".mysql_error());
						}
					}
					else {
						array_unshift($log, $array_usuario['id']." no debe actualizarce");
					}
				}
			}
			else {
				array_unshift($log, "no se agregó ".$array_usuario['id']." por que esta filtrado");
			}
		}
		else {
			array_unshift($log, "no se agregó ".$array_usuario['id'].", por que este ya existe");
		}
	}
	return $log;
}
?>