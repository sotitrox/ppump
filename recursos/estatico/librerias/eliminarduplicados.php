<?php
/************************************************************************\
*
*    PPump 0.3.1 Copyright 2014 Sotitrox
*    sotitrox@autistici.org
*
*    This file is part of PPump.
*
*    PPump is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    PPump is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
*
*    Public Pump o Pump Publico es programa escrito en PHP que permite
*    interpretar el canal web publico de la red Pump.io desde el sitio
*    ofirehose.com (https://ofirehose.com/feed.json) en formato Json 
*    para su fácil lectura.
*    Para información de su uso visite:
*    http://wiki.redaustral.tk/wikka.php?wakka=PPump
*
*
\************************************************************************/
function limpiarDuplicados($con) {
	$log = array();
	array_unshift($log, "Inicia la limpieza de usuarios duplicados en la base de datos.");
	$qdup = "select a1.num, a1.id from puser a1 inner join puser a2 on a1.id = a2.id and a1.num > a2.num";
	array_unshift($log, "Se inicia la busqueda de usuarios duplicados en la base de datos.");
	$qdupe = mysql_query($qdup, $con);
	
	$dupcant = mysql_num_rows($qdupe);
	print_r($duplicado);
	array_unshift($log, "Se encontraron ".$dupcant." usuarios duplicados.");
	while($duplicado = mysql_fetch_array($qdupe)) {
		$qbor = "delete from puser where num=".$duplicado['num'];
		$borrar = mysql_query($qbor, $con);
		if($borrar) {
			array_unshift($log, "Se ha borrado el usuario duplicado: ".$duplicado['id']);
		}
		else {
			array_unshift($log, "No se ha borrado el usuario duplicado ".$duplicado['id'].", el error:".mysql_error());
		}
	}
	array_unshift($log, "Finaliza la limpieza de usuarios duplicados en la base de datos.");
	return array_reverse($log);
}
?>