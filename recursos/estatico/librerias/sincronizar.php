<?php
/************************************************************************\
*
*    PPump 0.3.1 Copyright 2014 Sotitrox
*    sotitrox@autistici.org
*
*    This file is part of PPump.
*
*    PPump is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    PPump is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
*
*    Public Pump o Pump Publico es programa escrito en PHP que permite
*    interpretar el canal web publico de la red Pump.io desde el sitio
*    ofirehose.com (https://ofirehose.com/feed.json) en formato Json 
*    para su fácil lectura.
*    Para información de su uso visite:
*    http://wiki.redaustral.tk/wikka.php?wakka=PPump
*
*
\************************************************************************/
function sincronizarBD($bdusuarios, $con) {
	$log = array();
	$numbd = count($bdusuarios);
	for($y=0;$y<$numbd;$y++) {
		array_unshift($log, "Se inicia la recuperacion de datos de ".$bdusuarios[$y]);
		$ujson = ConectorPump::obtenerJson_esp($bdusuarios[$y]);
		if($ujson['ddf']['cURL obtenerJson']['http_code'] != 200 && !$ujson['ddf']['cURL obtenerJson']['errno']) {
			array_unshift($log, "Error al obtener datos de ".$ujson['ddf']['cURL obtenerJson']['url']." El servidor responde con codigo ".$ujson['ddf']['cURL obtenerJson']['http_code']);
		}
		elseif(!$ujson[0]) {
			array_unshift($log, "Error al obtener datos de ".$ujson['ddf']['cURL obtenerJson']['url']." ".$ujson['ddf']['cURL obtenerJson']['errno']." ".$ujson['ddf']['cURL obtenerJson']['error']);
		}
		else {
			$usuarios = $ujson[0];
			$numusu = count($usuarios);
			$ususdb = array();
			$iddb = array();
			$sql = mysql_query("select id, alias, avatar, lugar, creado, actualizado, seguidores, url, acerca  from puser",$con) or die("Problemas en el select1:".mysql_error());
			while($sql_users = mysql_fetch_array($sql)) {
				$usudb = array(
					'id' => $sql_users['id'],
					'actualizado' => $sql_users['actualizado']
				);
				array_unshift($ususdb, $usudb);
				array_unshift($iddb, $sql_users['id']);
			}
			for($x=0;$x<$numusu;$x++) {
				$coincidencia = array_search($usuarios[$x]['id'], $iddb, true);
				if($coincidencia === false) {
					array_unshift($log, "El usuario ".$usuarios[$x]['id']." no esta en la base de datos");
					$sql = "INSERT INTO puser (id, alias, avatar, lugar, creado, actualizado, seguidores, url, acerca) ";
					$sql.= "VALUES ('".$usuarios[$x]['id']."','".$usuarios[$x]['alias']."','".$usuarios[$x]['avatar']."','".$usuarios[$x]['lugar']."','".$usuarios[$x]['creado']."','".$usuarios[$x]['actualizado']."','".$usuarios[$x]['seguidores']."','".$usuarios[$x]['url']."','".$usuarios[$x]['acerca']."')";
					$insertar = mysql_query($sql);
					if($insertar) {
						array_unshift($log, "se agregó ".$usuarios[$x]['id']);
					}
					else {
						array_unshift($log, "error al agregar a ".$usuarios[$x]['id']." a la base de datos".mysql_error());
					}
				}
				else {
					array_unshift($log, "El usuario ".$usuarios[$x]['id']." esta en la base de datos");
					$actualizadodb = strtotime($ususdb[$coincidencia]['actualizado']);
					$actualizado = strtotime($usuarios[$x]['actualizado']);
					if($actualizadodb < $actualizado) {
						array_unshift($log, $ususdb[$coincidencia]['id']." debe ser actualizado");
						$sql = "UPDATE puser SET id='".$usuarios[$x]['id']."', alias='".$usuarios[$x]['alias']."', avatar='".$usuarios[$x]['avatar']."', lugar='".$usuarios[$x]['lugar']."', creado='".$usuarios[$x]['creado']."', actualizado='".$usuarios[$x]['actualizado']."', seguidores='".$usuarios[$x]['seguidores']."', url='".$usuarios[$x]['url']."', acerca='".$usuarios[$x]['acerca']."' ";
						$sql.= "WHERE id='".$ususdb[$coincidencia]['id']."'";
						$actualizar = mysql_query($sql);
						if($actualizar) {
							array_unshift($log, $ususdb[$coincidencia]['id']." actualizado.");
						}
						else {
							array_unshift($log, "error al actualizar a ".$ususdb[$coincidencia]['id']." a la base de datos".mysql_error());
						}
					}
					elseif($actualizadodb >= $actualizado) {
						array_unshift($log, $ususdb[$coincidencia]['id']." esta actualizado");
					}
				}
			}
		}
	}
array_unshift($log, "Terminó la sincronización de la base de datos.");
return array_reverse($log);
}
?>