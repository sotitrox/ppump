<?php
/************************************************************************\
*
*    PPump 0.3.1 Copyright 2014 Sotitrox
*    sotitrox@autistici.org
*
*    This file is part of PPump.
*
*    PPump is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    PPump is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
*
*    Public Pump o Pump Publico es programa escrito en PHP que permite
*    interpretar el canal web publico de la red Pump.io desde el sitio
*    ofirehose.com (https://ofirehose.com/feed.json) en formato Json 
*    para su fácil lectura.
*    Para información de su uso visite:
*    http://wiki.redaustral.tk/wikka.php?wakka=PPump
*
*
\************************************************************************/
function idioma($dir, $pre) {
	$idiomas = array();
	$directorio = opendir($dir);
	while ($archivo = readdir($directorio)) {
   	if ($archivo!="." && $archivo!="..") {
      array_unshift($idiomas, $archivo);
		}
	}
	if(!$_GET['i']) {
		$idiomasu = explode(',',$_SERVER['HTTP_ACCEPT_LANGUAGE']);
		$count = count($idiomasu);
		for($x=0;$x < $count; $x++) {
			$coincidencia = array_search(substr($idiomasu[$x],0,2), $idiomas, true);
			if($coincidencia === false) {
				$idioma = $pre;
			}
			else {
				$idioma = $idiomas[$coincidencia];
				$x = $count;
			}
		}
	}
	else {
		$coincidencia = array_search(substr($_GET['i'],0,2), $idiomas, true);
		if($coincidencia === false) {
			$idioma = $pre;
		}
		else {
			$idioma = $_GET['i'];
			$x = $count;
		}
	}
return $idioma;
}
?>