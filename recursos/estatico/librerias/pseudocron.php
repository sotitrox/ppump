<?php
/************************************************************************\
*
*    PPump 0.3.1 Copyright 2014 Sotitrox
*    sotitrox@autistici.org
*
*    This file is part of PPump.
*
*    PPump is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    PPump is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
*
*    Public Pump o Pump Publico es programa escrito en PHP que permite
*    interpretar el canal web publico de la red Pump.io desde el sitio
*    ofirehose.com (https://ofirehose.com/feed.json) en formato Json 
*    para su fácil lectura.
*    Para información de su uso visite:
*    http://wiki.redaustral.tk/wikka.php?wakka=PPump
*
*
\************************************************************************/
function pseudoCron($funcion, $intervalo, $columna, $con) {
	$log = array();
	$php=phpversion();
	$php=substr($php,0,3);
	if ($php >= "5.3") {
		$ahora = date_create();
		$segundos = date_timestamp_get($ahora);
	}
	else {
		$ahora = new DateTime();
		$segundos = $ahora->format("U");
	}
	$sql = mysql_query("select ".$columna." from eventos",$con) or die("Problemas en el select:".mysql_error());
	while($sql_users = mysql_fetch_array($sql)) {
		$tejec = strtotime($sql_users[$columna]) + $intervalo;
		if($tejec <= $segundos) {
			array_unshift($log, "Se ejecuta la función.");
			array_unshift($log, call_user_func_array($funcion[0], $funcion[1]));
			$sql1 = "UPDATE eventos SET ".$columna."='".date("Y-m-d H:i:s", $segundos)."'";
			$actualizar = mysql_query($sql1);
			if($actualizar) {
				array_unshift($log, "Actualizado...");
			}
			else {
				array_unshift($log, "No se pudo actualizar ".mysql_error());
			}
		}
		else {
			array_unshift($log, $tejec." ".$segundos."No se ejecuta, la diferencia es: ".($segundos-$tejec). date_default_timezone_get());
		}
	}
	return array_reverse($log);
}
?>