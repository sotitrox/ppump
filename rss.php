<?php
/************************************************************************\
*
*    PPump 0.3.1 Copyright 2014 Sotitrox
*    sotitrox@autistici.org
*
*    This file is part of PPump.
*
*    PPump is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    PPump is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
*
*    Public Pump o Pump Publico es programa escrito en PHP que permite
*    interpretar el canal web publico de la red Pump.io desde el sitio
*    ofirehose.com (https://ofirehose.com/feed.json) en formato Json 
*    para su fácil lectura.
*    Para información de su uso visite:
*    http://wiki.redaustral.tk/wikka.php?wakka=PPump
*
*
\************************************************************************/
require_once("sistema/configuracion.php");
require_once("recursos/estatico/librerias/conectorpump.php");
$rssfeed = '<?xml version="1.0" encoding="'.$CONCAR.'"?>';
$rssfeed .= '<rss version="2.0">';
$rssfeed .= '<channel>';
$rssfeed .= '<title>'.$TITULO.'</title>';
$rssfeed .= '<link>'.$BASE.'</link>';
$rssfeed .= '<description>Últimos posts públicos en la red Pump.io</description>';  
$ojson = ConectorPump::obtenerJson('https://ofirehose.com/feed.json');
if($ojson['ddf']['cURL obtenerJson']['http_code'] != 200) {
	$php=phpversion();
	$php=substr($php,0,3);
	if ($php >= "5.3") {
		$fecha = date_create();
		$segundos = date_timestamp_get($fecha);
	}
	else {
		$fecha = new DateTime();
		$segundos = $fecha->format("U");
	}
	$rssfeed .= '<item>';
   $rssfeed .= '<author>PPump</author>';
   $rssfeed .= '<title>' . $TITULO . '</title>';
   $rssfeed .= '<description>'.htmlentities('Imposible obtener datos de '.$ojson['ddf']['cURL obtenerJson']['url']."\nError ".$ojson['ddf']['cURL obtenerJson']['errno'] .' - '.$ojson['ddf']['cURL obtenerJson']['error'], ENT_QUOTES, "utf-8"). '</description>';
   $rssfeed .= '<link>' . $BASE . '</link>';
   $rssfeed .= '<pubDate>' . date("D, d M Y H:i:s O", $segundos) . '</pubDate>';
   $rssfeed .= '</item>';
   $rssfeed .= '</channel>';
	$rssfeed .= '</rss>';
	echo $rssfeed;
	exit;
}
else {
	$response = $ojson[0];
}
if(!$cron) {
	require_once("recursos/estatico/librerias/sincronizar.php");
	require_once("recursos/estatico/librerias/actualizar.php");
	require_once("recursos/estatico/librerias/pseudocron.php");
	if($response) {
		pseudoCron(array('actualizarBD', array($response, $filtro, $con)), $interact, "usudb", $con);
		pseudoCron(array('sincronizarBD', array($bdusuarios, $con)), $intersin, "sinc", $con);
	}
}
if($_GET['filtro']) {
	$filtro = array_merge($filtro, explode("%2C", rawurlencode($_GET['filtro'])));
}
$tamanio = count($response['items']);
for ($x=0;$x<$tamanio; $x++) {
	foreach($filtro as $val) {
		$coincidencia = strpos(rawurlencode(str_replace("acct:", "", $response['items']["$x"]['actor']['id'])), $val);
		if($coincidencia === false) {
			if(($response['items'][$x-1]['verb'] == 'update' && $response['items'][$x]['object']['id'] == $response['items'][$x-1]['object']['id']) || ($response['items'][$x+1]['verb'] == 'update' && $response['items'][$x]['object']['id'] == $response['items'][$x+1]['object']['id'])) {					
				$mostrar = 0;
				break;
			}
			else {
				$mostrar = 1;
			}
		}
		else {
			$mostrar = 0;
			break;
		}
	}
	if(($response['items']["$x"]['verb']=='post' || $response['items']["$x"]['verb']=='update') && $mostrar) {
		$avatar=$response['items']["$x"]['actor']['image']['url'];
		$actorid = str_replace("acct:", "", $response['items']["$x"]['actor']['id']);
		$nombre=$response['items']["$x"]['actor']['displayName'];
		$actorurl=$response['items']["$x"]['actor']['url'];
		$mensaje=$response['items']["$x"]['object']['content'];
		$fecha=$response['items']["$x"]['object']['published'];
		$url=$response['items']["$x"]['object']['url'];
		$generador=$response['items']["$x"]['generator']['displayName'];
		$actorlocation = $response['items']["$x"]['actor']['location']['displayName'];
		$id = $response['items']["$x"]['object']['id'];
		$mensaje .= '<br>---<br><b><a href="'.$actorurl.'">'.$nombre.'</a></b> <a href="https://pump2rss.com/feed/'.$actorid.'.atom"><img src="'.$BASE.'recursos/estatico/graficos/rss-mini.png" alt="Atom" title="Atom"></a><br><address>'.$actorid.'<br>'.$actorlocation.'</address>';
		if($response['items']["$x"]['object']['objectType'] == 'note') {
			if($response['items']["$x"]['object']['displayName']) {
				$titulo = $response['items']["$x"]['object']['displayName'];
			}
			else {
				$titulo = substr(strip_tags($mensaje), 0, 30)."...";
			}
			$mensaje = htmlspecialchars($mensaje, ENT_QUOTES, "utf-8");
			#$mensaje = addslashes($mensaje);
			$rssfeed .= '<item>';
        	$rssfeed .= '<author>' . $nombre . '</author>';
        	$rssfeed .= '<title>' . $titulo . '</title>';
        	$rssfeed .= '<description>' . $mensaje . '</description>';
        	$rssfeed .= '<link>' . $url . '</link>';
        	$rssfeed .= '<guid>' . $id . '</guid>';
        	$rssfeed .= '<pubDate>' . date("D, d M Y H:i:s O", strtotime($fecha)) . '</pubDate>';
        	$rssfeed .= '</item>';
		}
		elseif($response['items']["$x"]['object']['objectType'] == 'image') {
			if($response['items']["$x"]['object']['displayName']) {
				$titulo = $response['items']["$x"]['object']['displayName'];
			}
			else {
				$titulo = "Imagen";
			}
			$imagen = $response['items']["$x"]['object']['fullImage']['url'];
			#$mensaje = addslashes('<img src=' . $imagen . ' width=300><br>'. $mensaje);
			$mensaje = htmlspecialchars('<img src=' . $imagen . ' width=300><br>'. $mensaje, ENT_QUOTES, "utf-8");
			$rssfeed .= '<item>';
        	$rssfeed .= '<author>' . $nombre . '</author>';
        	$rssfeed .= '<title>' . $titulo . '</title>';
        	$rssfeed .= '<description>' . $mensaje . '</description>';
        	$rssfeed .= '<link>'.$url . '</link>';
        	$rssfeed .= '<guid>' . $id . '</guid>';
        	$rssfeed .= '<pubDate>' . date("D, d M Y H:i:s O", strtotime($fecha)) . '</pubDate>';
        	$rssfeed .= '</item>';
		}
	}
}

$rssfeed .= '</channel>';
$rssfeed .= '</rss>';
echo $rssfeed;
?>